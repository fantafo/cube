﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pvr_UnitySDKAPI;

public class BoardController : AutoCommander
{
    private int clickCount;
    public bool isEnd;
    public GameObject[] TextObejct;
    public AudioSource[] typeSound;
    public GameObject AudioPlayer;
    public bool isCoinEnd;
    public bool TypeStartFlag;

    private Coroutine updown;
    void Start()
    {
        clickCount = 0;
        isEnd = true;
        isCoinEnd = false;
        TypeStartFlag = false;
        updown = StartCoroutine("AssembleUpDown");
    }

    void Update()
    {
        if (isCoinEnd)
        {
            if(TypeStartFlag == false)
            {
                TypeStartFlag = true;
                touchBoard();
            }
            if ((VRInput.ClickButtonUp || Input.GetMouseButtonUp(1) || Controller.UPvr_GetKeyUp(0, Pvr_KeyCode.TRIGGER)) && ControllerDirHelper.isUp)
            {
                Ray ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(FTFReticlePosition.Value));
                RaycastHit hit;
                Physics.Raycast(ray, out hit);

                if (Physics.Raycast(ray, out hit) && hit.transform.gameObject == gameObject) // 레이 검출을 하고
                {
                    Broadcast("touchBoard");
                }
            }
        }
    }

    [OnCommand]
    public void touchBoard()
    {
        if (isEnd)
        {

            switch (clickCount)
            {
                case 0:
                    AudioPlayer.GetComponent<SoundPlayer>().AudioPlay_1();
                    TextObejct[0].SetActive(true);
                    break;
                case 1:
                    AudioPlayer.GetComponent<SoundPlayer>().AudioPlay_1();
                    TextObejct[0].SetActive(false);
                    TextObejct[1].SetActive(true);
                    break;
                case 2:
                    AudioPlayer.GetComponent<SoundPlayer>().AudioPlay_1();
                    TextObejct[1].SetActive(false);
                    TextObejct[2].SetActive(true);
                    break;
                case 3:

                    break;
                default:
                    break;
                   
            }
            clickCount++;
            isEnd = false;
        }
        else
        {
            if (clickCount - 1 < 3)
            {
                TextObejct[clickCount - 1].GetComponent<TypeWriterEffect>().EndEffect();
            }

        }
    }

    private IEnumerator AssembleUpDown()
    {
        float updown = 0.0f;
        while (true)
        {
            gameObject.transform.position += new Vector3(0.0f, Mathf.Sin(updown) * 0.002f, 0.0f);
            updown += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }

}
