﻿using FTF;
using FTF.Packet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayButton : AutoCommander
{

    public Animator cubeAnimator;
    private int count;
    public GameObject BoxEffect;
    public GameObject BoxEffect2;
    public AudioSource electric;

    public GameObject aasdfasdf;
    public GameObject EndEffect;

    private Coroutine endcoroutine;
    private Coroutine updownCor;

    private void Start()
    {
        cubeAnimator = gameObject.GetComponent<Animator>();
        cubeAnimator.SetBool("start", false);
        cubeAnimator.SetBool("1", false);
        cubeAnimator.SetBool("2", false);
        cubeAnimator.SetBool("3", false);
        count = -1;
        updownCor = StartCoroutine("AssembleUpDown");
    }

    public void pushAnimationButton()
    {
        Broadcast("ActionCommand");   
    }
    
    [OnCommand]
    public void ActionCommand()
    {
        gameObject.GetComponent<Animator>().enabled = true;
        if (count == -1)
        {
            StopCoroutine(updownCor);
            BoxEffect.SetActive(true);
            BoxEffect2.SetActive(true);
            electric.Play();
            cubeAnimator.SetBool("start", true);
            count++;
        }
        else if (count == 0)
        {
            if(BoxEffect.activeSelf)
            {
                BoxEffect.SetActive(false);
                BoxEffect2.SetActive(false);
            }
            cubeAnimator.SetBool("1", true);
            cubeAnimator.SetBool("start", false);
            cubeAnimator.SetBool("3", false);
            count++;
        }
        else if (count == 1)
        {
            cubeAnimator.SetBool("2", true);
            cubeAnimator.SetBool("1", false);
            endcoroutine = StartCoroutine("endEffect");
            StartCoroutine("ShowArrow");
            count++;
        }
        else if (count == 2)
        {
            cubeAnimator.SetBool("3", true);
            cubeAnimator.SetBool("2", false);
            count = 0;
        }
    }

    IEnumerator endEffect()
    {
        yield return new WaitForSeconds(5.0f);
        EndEffect.SetActive(true);

        yield return null;
    }
    IEnumerator ShowArrow()
    {
        yield return new WaitForSeconds(3.0f);
        aasdfasdf.SetActive(true);
    }



    private IEnumerator AssembleUpDown()
    {
        float updown = 0.0f;
        while (true)
        {
            gameObject.transform.position += new Vector3(0.0f, Mathf.Sin(updown) * 0.02f, 0.0f);
            updown += 0.03f;
            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }
}
