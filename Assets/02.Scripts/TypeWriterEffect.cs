﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeWriterEffect : MonoBehaviour {

    public TextMesh txt;
    public string story;
    public Coroutine TypeEffect;
    public GameObject Board;
    void Awake()
    {
        txt = GetComponent<TextMesh>();
        story = txt.text;
        txt.text = "";

        // TODO: add optional delay when to start
        TypeEffect = StartCoroutine("PlayText");
    }

    IEnumerator PlayText()
    {
        foreach (char c in story)
        {
            txt.text += c;

            float randTime =0.0f;
            Random r = new Random();
            for(int i = 0; i< 20; i++)
            {
                randTime += Random.Range(0.000f, 0.007f);
            }
            Board.GetComponent<BoardController>().typeSound[Random.Range(0, 2)].Play();

            yield return new WaitForSeconds(randTime);
        }
        Board.GetComponent<BoardController>().isEnd = true;
    }

    public void EndEffect()
    {
        StopCoroutine("PlayText");
        txt.text = story;

        Board.GetComponent<BoardController>().isEnd = true;
    }
}
