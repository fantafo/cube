﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour {

    // 큐브의 머티리얼을 바꿔준다. 

    public List<Material> mymaterial = new List<Material>();

	// Use this for initialization
	void Start ()
    {
        //MaterialChange();
    }


    public void MaterialChange(int materialNum)
    {
        gameObject.GetComponent<Renderer>().sharedMaterial = mymaterial[materialNum];
    }
    
}
