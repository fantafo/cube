﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartFadeIn : MonoBehaviour {

    // Use this for initialization
    public AudioSource sound;
    public AudioSource BGM;
    void Start () {
        SetFadeSphereColorA(1.0f);
        StartCoroutine("FadeInRoutine");
    }
    
    // Update is called once per frame
    void Update ()
    {
	}

    IEnumerator FadeInRoutine()
    {
        yield return new WaitForSeconds(3.0f);
        sound.Play();
        SetFadeSphereColorA(0.80f);
        yield return new WaitForSeconds(0.5f);
        sound.Play();
        SetFadeSphereColorA(0.60f);
        yield return new WaitForSeconds(0.5f);
        sound.Play();
        SetFadeSphereColorA(0.35f);
        yield return new WaitForSeconds(0.5f);
        sound.Play();
        SetFadeSphereColorA(0.0f);
        BGM.Play();
        yield return new WaitForSeconds(1.0f);


        gameObject.SetActive(false);
    }



    private void SetFadeSphereColorA(float ColorA)
    {
        if (gameObject)
            gameObject.GetComponent<Renderer>().material.SetColor("_Color", new Color(0, 0, 0, ColorA));
    }
}
