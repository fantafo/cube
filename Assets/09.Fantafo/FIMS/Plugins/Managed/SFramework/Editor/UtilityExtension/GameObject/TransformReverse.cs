﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class TransformReverse : BaseExtensionEditor
    {
        [MenuItem("GameObject/Hierarchy/Reverse")]
        public static void TransformReverseToHierarchy()
        {
            if (!init("Reverse", 2))
                return;
            if (transforms == null || transforms.Length == 0)
                return;

            Transform parent = transforms[0].parent;
            if (!parent)
            {
                Debug.LogWarning("Root 오브젝트는 반전할 수 없습니다.");
                return;
            }

            foreach (var t in transforms)
                if (t.parent != parent)
                {
                    Debug.LogWarning("같은 부모의 오브젝트만 반전할 수 있습니다.");
                    return;
                }

            Sort(t => t.GetSiblingIndex());

            int idx = transforms[0].GetSiblingIndex();
            foreach (var t in transforms)
            {
                if (t.GetSiblingIndex() != idx++)
                {
                    Debug.LogWarning("연결되어 선택된 오브젝트만 반전할 수 있습니다.");
                    return;
                }
            }

            idx = transforms[0].GetSiblingIndex();
            foreach (var t in transforms)
            {
                t.SetSiblingIndex(idx);
            }
            Debug.Log(transforms[0].GetSiblingIndex());

        }


    }
}