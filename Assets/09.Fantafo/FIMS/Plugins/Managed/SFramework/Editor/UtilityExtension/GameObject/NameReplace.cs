﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using EGL = UnityEditor.EditorGUILayout;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions
{
    public class ObjectNameReplaceWindow : EditorWindow
    {

        [MenuItem("GameObject/Naming/Replace")]
        public static void Showing()
        {
            GetWindowWithRect<ObjectNameReplaceWindow>(new Rect(0, 0, 300, 80), true, "NameReplace", true);
        }

        public void OnEnable()
        {
            target = EditorPrefs.GetString("ObjectNameReplaceWindowTarget", "A");
            replace = EditorPrefs.GetString("ObjectNameReplaceWindowReplace", "B");
        }

        public string target;
        public string replace;

        public void OnGUI()
        {
            EGL.Space();
            target = EGL.TextField("Old", target);
            replace = EGL.TextField("Replace", replace);
            if (GUI.changed)
            {
                EditorPrefs.SetString("ObjectNameReplaceWindowTarget", target);
                EditorPrefs.SetString("ObjectNameReplaceWindowReplace", replace);
            }
            EGL.Space();

            EGL.BeginHorizontal();
            EditorGUILayout.Space();
            if (GUILayout.Button("Change", GUILayout.Width(100)))
            {
                Transform[] trans = Selection.transforms;
                Array.Sort(trans, (a, b) =>
                {
                    return a.GetSiblingIndex().CompareTo(b.GetSiblingIndex());
                });

                for (int i = 0; i < trans.Length; i++)
                {
                    trans[i].name = trans[i].name.Replace(target, replace);
                }
            }
            EGL.EndHorizontal();
        }

    }
}