﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Sions.Editors;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class TransformRotate : EditorWindow
    {
        [MenuItem("GameObject/Transform/Rotate Around")]
        public static void Showing()
        {
            EditorWindow.GetWindow<TransformRotate>();
        }

        public Transform pivot;
        public Vector3 axis = Vector3.up;
        public float angle = 90;
        public Vector3 offset = Vector3.forward;
        public bool lookPivot;
        public Vector3 lookOffset = Vector3.forward;
        public Transform[] targets = new Transform[1];

        public void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            GUILayout.Space(10);
            pivot = (Transform)EditorGUILayout.ObjectField("Pivot", pivot, typeof(Transform), true);
            axis = EditorGUILayout.Vector3Field("Axis", axis);
            GUILayout.BeginHorizontal();
            {
                angle = EditorGUILayout.FloatField("Angle", angle);
                if(GUILayout.Button("Calc", GUILayout.Width(50)))
                {
                    angle = 360f / targets.Length;
                    Repaint();
                    return;
                }
            }
            GUILayout.EndHorizontal();
            offset = EditorGUILayout.Vector3Field("Offset", offset);

            if(lookPivot = EditorGUILayout.Toggle("Look Pivot", lookPivot))
            {
                lookOffset = EditorGUILayout.Vector3Field("   LookOffset", lookOffset);
            }
            GUILayout.Space(10);
            
            int newCount = EditorGUILayout.DelayedIntField("    Count", targets.Length);
            if (targets.Length != newCount)
                Array.Resize(ref targets, newCount);

            for(int i=0; i<targets.Length; i++)
            {
                var newTransform = (Transform)EditorGUILayout.ObjectField("      - "+i, targets[i], typeof(Transform), true); ;
                if (newTransform != targets[i])
                {
                    if (!newTransform)
                    {
                        targets = targets.RemoveAt(i--);
                    }
                    else
                    { 
                        targets[i] = newTransform;
                        if (Selection.gameObjects.Length > 1)
                        {
                            targets = new Transform[Selection.gameObjects.Length];
                            for (int j = 0; j < targets.Length; j++)
                            {
                                targets[j] = Selection.gameObjects[j].transform;
                            }
                            Repaint();
                            break;
                        }
                    }
                }
            }

            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Sort-Name"))
                {
                    Array.Sort(targets, (a, b) => a.name.CompareTo(b.name));
                }
                if (GUILayout.Button("Sort-Name-Inv"))
                {
                    Array.Sort(targets, (a, b) => b.name.CompareTo(a.name));
                }
                if (GUILayout.Button("Sort-Sibling"))
                {
                    Array.Sort(targets, (a, b) => a.GetSiblingIndex().CompareTo(b.GetSiblingIndex()));
                }
                if (GUILayout.Button("Sort-Sibling-Inv"))
                {
                    Array.Sort(targets, (a, b) => b.GetSiblingIndex().CompareTo(a.GetSiblingIndex()));
                }
            }
            GUILayout.EndHorizontal();
            if (GUILayout.Button("Apply"))
            {
                for(int i=0; i<targets.Length; i++)
                {
                    targets[i].position = pivot.position + Quaternion.AngleAxis(angle * i, axis) * offset;

                    if(lookPivot)
                    {
                        targets[i].rotation = Quaternion.LookRotation(pivot.position - targets[i].position) * Quaternion.Euler(lookOffset);
                    }
                }
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

    }
}