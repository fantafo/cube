﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Reflection;
using System.IO;
using System.Text;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions.Editors
{
    public class ClearLineEnding
    {
        [MenuItem("Sions/Tools/Clear LineEnding")]
        public static void ToggleSimulationMode()
        {
            Search(Application.dataPath);
            Debug.Log("Complete");
        }

        private static void Search(string path)
        {
            foreach (var dir in Directory.GetDirectories(path))
            {
                Search(dir);
            }

            foreach (var filePath in Directory.GetFiles(path))
            {
                if (filePath.EndsWith(".cs", StringComparison.OrdinalIgnoreCase))
                {
                    EditLineEnding(filePath);
                }
            }
        }

        private static void EditLineEnding(string filePath)
        {
            try
            {
                filePath = filePath.Replace('\\', '/');
                if (!File.Exists(filePath))
                    return;

                string[] text = File.ReadAllLines(filePath);

                StringBuilder sb = new StringBuilder();
                foreach (var t in text)
                {
                    sb.Append(t).Append("\r\n");
                }

                sb.Remove(sb.Length - 2, 2);

                File.WriteAllText(filePath, sb.ToString(), Encoding.UTF8);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
    }
}