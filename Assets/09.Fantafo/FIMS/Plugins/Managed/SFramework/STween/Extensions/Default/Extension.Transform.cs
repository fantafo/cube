﻿using UnityEngine;

public static class STweenTransformExtension
{
    #region only To
    public static STweenState twnMove(this Transform trans, Vector3 to, float duration)
    {
        return STween.move(trans, to, duration);
    }
    public static STweenState twnMoveLocal(this Transform trans, Vector3 to, float duration)
    {
        return STween.moveLocal(trans, to, duration);
    }
    public static STweenState twnMoveX(this Transform trans, float to, float duration)
    {
        return STween.moveX(trans, to, duration);
    }
    public static STweenState twnMoveY(this Transform trans, float to, float duration)
    {
        return STween.moveY(trans, to, duration);
    }
    public static STweenState twnMoveZ(this Transform trans, float to, float duration)
    {
        return STween.moveZ(trans, to, duration);
    }
    public static STweenState twnMoveLocalX(this Transform trans, float to, float duration)
    {
        return STween.moveLocalX(trans, to, duration);
    }
    public static STweenState twnMoveLocalY(this Transform trans, float to, float duration)
    {
        return STween.moveLocalY(trans, to, duration);
    }
    public static STweenState twnMoveLocalZ(this Transform trans, float to, float duration)
    {
        return STween.moveLocalZ(trans, to, duration);
    }

    public static STweenState twnRotate(this Transform comp, Vector3 to, float duration)
    {
        return STween.rotate(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocal(this Transform comp, Vector3 to, float duration)
    {
        return STween.rotateLocal(comp.transform, to, duration);
    }
    public static STweenState twnRotate(this Transform comp, Quaternion to, float duration)
    {
        return STween.rotate(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocal(this Transform comp, Quaternion to, float duration)
    {
        return STween.rotateLocal(comp.transform, to, duration);
    }
    public static STweenState twnRotateQ(this Transform comp, Vector3 to, float duration)
    {
        return STween.rotateQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalQ(this Transform comp, Vector3 to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateQ(this Transform comp, Quaternion to, float duration)
    {
        return STween.rotateQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalQ(this Transform comp, Quaternion to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, to, duration);
    }

    public static STweenState twnRotateX(this Transform trans, float to, float duration)
    {
        return STween.rotateX(trans, to, duration);
    }
    public static STweenState twnRotateY(this Transform trans, float to, float duration)
    {
        return STween.rotateY(trans, to, duration);
    }
    public static STweenState twnRotateZ(this Transform trans, float to, float duration)
    {
        return STween.rotateZ(trans, to, duration);
    }
    public static STweenState twnRotateLocalX(this Transform trans, float to, float duration)
    {
        return STween.rotateLocalX(trans, to, duration);
    }
    public static STweenState twnRotateLocalY(this Transform trans, float to, float duration)
    {
        return STween.rotateLocalY(trans, to, duration);
    }
    public static STweenState twnRotateLocalZ(this Transform trans, float to, float duration)
    {
        return STween.rotateLocalZ(trans, to, duration);
    }

    public static STweenState twnScale(this Transform trans, Vector3 to, float duration)
    {
        return STween.scale(trans, to, duration);
    }
    public static STweenState twnScaleX(this Transform trans, float to, float duration)
    {
        return STween.scaleX(trans, to, duration);
    }
    public static STweenState twnScaleY(this Transform trans, float to, float duration)
    {
        return STween.scaleY(trans, to, duration);
    }
    public static STweenState twnScaleZ(this Transform trans, float to, float duration)
    {
        return STween.scaleZ(trans, to, duration);
    }

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnSize(this Transform trans, Vector2 to, float duration)
    {
        return STween.size(trans as RectTransform, to, duration);
    }
    public static STweenState twnSizeX(this Transform trans, float to, float duration)
    {
        return STween.sizeX(trans as RectTransform, to, duration);
    }
    public static STweenState twnSizeY(this Transform trans, float to, float duration)
    {
        return STween.sizeY(trans as RectTransform, to, duration);
    }

    public static STweenState twnAnchored(this Transform trans, Vector3 to, float duration)
    {
        return STween.anchored(trans as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredX(this Transform trans, float to, float duration)
    {
        return STween.anchoredX(trans as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredY(this Transform trans, float to, float duration)
    {
        return STween.anchoredY(trans as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredZ(this Transform trans, float to, float duration)
    {
        return STween.anchoredZ(trans as RectTransform, to, duration);
    }
#endif

    public static STweenState twnAlpha(this Transform trans, float to, float duration)
    {
        return STween.alpha(trans, to, duration);
    }
    public static STweenState twnColor(this Transform trans, Color to, float duration)
    {
        return STween.color(trans, to, duration);
    }
    #endregion

    #region From, To
    public static STweenState twnMove(this Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return STween.move(trans, from, to, duration);
    }
    public static STweenState twnMoveLocal(this Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return STween.moveLocal(trans, from, to, duration);
    }
    public static STweenState twnMoveX(this Transform trans, float from, float to, float duration)
    {
        return STween.moveX(trans, from, to, duration);
    }
    public static STweenState twnMoveY(this Transform trans, float from, float to, float duration)
    {
        return STween.moveY(trans, from, to, duration);
    }
    public static STweenState twnMoveZ(this Transform trans, float from, float to, float duration)
    {
        return STween.moveZ(trans, from, to, duration);
    }
    public static STweenState twnMoveLocalX(this Transform trans, float from, float to, float duration)
    {
        return STween.moveLocalX(trans, from, to, duration);
    }
    public static STweenState twnMoveLocalY(this Transform trans, float from, float to, float duration)
    {
        return STween.moveLocalY(trans, from, to, duration);
    }
    public static STweenState twnMoveLocalZ(this Transform trans, float from, float to, float duration)
    {
        return STween.moveLocalZ(trans, from, to, duration);
    }

    public static STweenState twnRotate(this Transform comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotate(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocal(this Transform comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateLocal(comp.transform, from, to, duration);
    }
    public static STweenState twnRotate(this Transform comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotate(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocal(this Transform comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateLocal(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateQ(this Transform comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalQ(this Transform comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateQ(this Transform comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalQ(this Transform comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, from, to, duration);
    }

    public static STweenState twnRotateX(this Transform trans, float from, float to, float duration)
    {
        return STween.rotateX(trans, from, to, duration);
    }
    public static STweenState twnRotateY(this Transform trans, float from, float to, float duration)
    {
        return STween.rotateY(trans, from, to, duration);
    }
    public static STweenState twnRotateZ(this Transform trans, float from, float to, float duration)
    {
        return STween.rotateZ(trans, from, to, duration);
    }
    public static STweenState twnRotateLocalX(this Transform trans, float from, float to, float duration)
    {
        return STween.rotateLocalX(trans, from, to, duration);
    }
    public static STweenState twnRotateLocalY(this Transform trans, float from, float to, float duration)
    {
        return STween.rotateLocalY(trans, from, to, duration);
    }
    public static STweenState twnRotateLocalZ(this Transform trans, float from, float to, float duration)
    {
        return STween.rotateLocalZ(trans, from, to, duration);
    }

    public static STweenState twnScale(this Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return STween.scale(trans, from, to, duration);
    }

    public static STweenState twnScaleX(this Transform trans, float from, float to, float duration)
    {
        return STween.scaleX(trans, from, to, duration);
    }
    public static STweenState twnScaleY(this Transform trans, float from, float to, float duration)
    {
        return STween.scaleY(trans, from, to, duration);
    }
    public static STweenState twnScaleZ(this Transform trans, float from, float to, float duration)
    {
        return STween.scaleZ(trans, from, to, duration);
    }

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnSize(this Transform trans, Vector2 from, Vector2 to, float duration)
    {
        return STween.size(trans as RectTransform, from, to, duration);
    }
    public static STweenState twnSizeX(this Transform trans, float from, float to, float duration)
    {
        return STween.sizeX(trans as RectTransform, from, to, duration);
    }
    public static STweenState twnSizeY(this Transform trans, float from, float to, float duration)
    {
        return STween.sizeY(trans as RectTransform, from, to, duration);
    }

    public static STweenState twnAnchored(this Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return STween.anchored(trans as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredX(this Transform trans, float from, float to, float duration)
    {
        return STween.anchoredX(trans as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredY(this Transform trans, float from, float to, float duration)
    {
        return STween.anchoredY(trans as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredZ(this Transform trans, float from, float to, float duration)
    {
        return STween.anchoredZ(trans as RectTransform, from, to, duration);
    }
#endif

    public static STweenState twnAlpha(this Transform trans, float from, float to, float duration)
    {
        return STween.alpha(trans, from, to, duration);
    }
    public static STweenState twnColor(this Transform trans, Color from, Color to, float duration)
    {
        return STween.color(trans, from, to, duration);
    }
    #endregion
}