﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 옵저버일경우에만 생성되는 컨트롤러로서,
/// 매 프레임마다 표현할 유저가 선생님인지 학생인지를 검사한다.
/// </summary>
public class TeacherObjectController : SMonoBehaviour
{
    static TeacherObjectController instance;
    public static TeacherObjectController main
    {
        get
        {
            if (instance == null)
                instance = new GameObject("TeacherObjectController").AddComponent<TeacherObjectController>();
            return instance;
        }
    }

    public List<TeacherObject> objs = new List<TeacherObject>();
    public bool isTeacher;

    private void Update()
    {
        for(int i=0; i<objs.Count; i++)
        {
            objs[i].Check(false);
        }
    }
}
