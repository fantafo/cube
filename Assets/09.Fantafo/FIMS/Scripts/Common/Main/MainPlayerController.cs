﻿using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 앱의 메인 플레이어를 지칭하는 컨트롤러다.
    /// MainPlayerController.main을 하게되면 어떻게 됐던 플레이어의 카메라를 찾을 수 있다.
    /// 이곳에서는 플레이어의 hand와 cameraRig을 담당한다.
    /// </summary>
    public class MainPlayerController : SMonoBehaviour
    {
        public static MainPlayerController main;

        public MainPlayerHand hand;
        public MainPlayerCameraRig cameraRig;
        public bool UseControllerModel = true;

        protected virtual void Awake()
        {
            main = this;
        }

        protected virtual void OnDrawGizmos()
        {
            float tall = 1.2f;
            if (!Application.isPlaying)
            {
                Gizmos.matrix = transform.localToWorldMatrix;
                Gizmos.DrawCube(new Vector3(0, (0.65f/1.6f)*tall), new Vector3(0.3f, (1.3f/1.6f)*tall, 0.2f));
                Gizmos.DrawCube(new Vector3(0, (1.5f/1.6f)*tall, 0.1f), new Vector3(0.3f, (0.3f/1.6f)*tall, 0.4f));
                Gizmos.DrawCube(new Vector3(0.2f, (1f/1.6f)*tall, 0), new Vector3(0.1f, (0.5f/1.6f)*tall, 0.1f));
                Gizmos.DrawCube(new Vector3(-0.2f, (1f/1.6f)*tall, 0), new Vector3(0.1f, (0.5f/1.6f)*tall, 0.1f));
            }
        }
    }
}