using UnityEngine;
using UnityEngine.UI;

namespace VRStandardAssets.Utils
{
    // The reticle is a small point at the centre of the screen.
    // It is used as a visual aid for aiming. The position of the
    // reticle is either at a default position in space or on the
    // surface of a VRInteractiveItem as determined by the VREyeRaycaster.
    // 레티클은 화면 중앙의 작은 점.
    // 조준을위한 시각 보조 도구로 사용. 
    // 레티클 위치는 공간의 기본 위치 또는
    // VREyRaycaster에 의해 결정된 VRInteractiveItem의 표면임.

    public class Reticle : MonoBehaviour
    {
        //레티클이 배치 된 카메라에서의 기본 거리.
        [SerializeField] private float m_DefaultDistance = 5f;      //레티클이 배치 된 카메라에서의 기본 거리.
        [SerializeField] private bool m_UseNormal;                  // 레티클을 서페이스와 평행하게 놓아야하는지 여부.
        [SerializeField] private Image m_Image;                     // 레티클 기본 이미지 (작은 점)
        [SerializeField] private Transform m_ReticleTransform;      // We need to affect the reticle's transform.
        [SerializeField] private Transform m_Camera;                // The reticle is always placed relative to the camera.

        private Vector3 m_OriginalScale;                            // Since the scale of the reticle changes, the original scale needs to be stored.
        private Quaternion m_OriginalRotation;                      // Used to store the original rotation of the reticle.

        // public 필드
        public bool UseNormal
        {
            get { return m_UseNormal; }
            set { m_UseNormal = value; }
        }

        public Transform ReticleTransform { get { return m_ReticleTransform; } }

        private void Awake()
        {
            // Store the original scale and rotation.
            // Reticle 초기 local 값 저장
            m_OriginalScale = m_ReticleTransform.localScale;
            m_OriginalRotation = m_ReticleTransform.localRotation;
        }

        // 레티클 이미지 숨기기
        public void Hide()
        {
            m_Image.enabled = false;
        }

        // 레티클 이미지 보이기
        public void Show()
        {
            m_Image.enabled = true;
        }

        // This overload of SetPosition is used when the the VREyeRaycaster hasn't hit anything.
        //이 SetPosition의 오버로드 함수는 VREyeRaycaster가 아무 것도 HIT하지 않았을 때 사용.
        public void SetPosition ()
        {
            // Set the position of the reticle to the default distance in front of the camera.
            m_ReticleTransform.position = m_Camera.position + m_Camera.forward * m_DefaultDistance;

            // Set the scale based on the original and the distance from the camera.
            m_ReticleTransform.localScale = m_OriginalScale * m_DefaultDistance;

            // The rotation should just be the default.
            m_ReticleTransform.localRotation = m_OriginalRotation;
        }


        // This overload of SetPosition is used when the VREyeRaycaster has hit something.
        // VREyeRaycaster가 무언가를 HIT하면 SetPosition의 오버로드 함수 사용.
        public void SetPosition (RaycastHit hit)
        {
            m_ReticleTransform.position = hit.point;
            m_ReticleTransform.localScale = m_OriginalScale * hit.distance;
            
            // If the reticle should use the normal of what has been hit...
            if (m_UseNormal)
                // ... set it's rotation based on it's forward vector facing along the normal.
                m_ReticleTransform.rotation = Quaternion.FromToRotation (Vector3.forward, hit.normal);
            else
                // However if it isn't using the normal then it's local rotation should be as it was originally.
                m_ReticleTransform.localRotation = m_OriginalRotation;
        }
    }
}