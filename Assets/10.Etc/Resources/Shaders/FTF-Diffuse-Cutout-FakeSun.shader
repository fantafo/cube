﻿Shader "FTF/Diffuse (Fake Sun)" 
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		[Space]
		_lightCol("Light Color", Color) = (1,1,1,1)
		_lightDir("Light Direction", Vector) = (0,1,3,1)
	}
	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 150

		CGPROGRAM
		#pragma surface surf SimpleLambert noforwardadd


		fixed4 _Color;
		fixed4 _lightCol;
		half4 _lightDir;
		sampler2D _MainTex;

        half4 LightingSimpleLambert (SurfaceOutput s, fixed3 lightDir, fixed atten)
		{/*
			float difLight = max(0, dot(s.Normal, lightDir));
			float4 col;
			col.rgb = s.Albedo * _LightColor0.rgb * (difLight * atten);
			col.a = s.Alpha;
			*/
			float difLight = max(0, dot(s.Normal, normalize(_lightDir)));
			float4 col;
			col.rgb = s.Albedo * _lightCol.rgb * (difLight * atten);
			col.a = s.Alpha;
			return col;
        }

		struct Input {
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = _Color.rgb * c.rgb;
			o.Alpha = _Color.a * c.a;
		}
		ENDCG
	}
	Fallback "Mobile/VertexLit"
}
